import React from 'react';
import {View, StyleSheet} from 'react-native';
import MapView from 'react-native-maps';
import {Marker} from 'react-native-maps';

const App = () => {
  var markers = [
    {
      latitude: 30.3753,
      longitude: 69.3451,
      title: 'Foo Place',
      subtitle: '1234 Foo Drive',
    },
  ];

  return (
    <View style={styles.mapStyles}>
      <MapView
        style={styles.mapStyles}
        initialRegion={{
          latitude: 30.3753,
          longitude: 69.3451,
          latitudeDelta: 0.0013,
          longitudeDelta: 0.0004,
        }}>
        <Marker
          coordinate={{
            latitude: 31.4504,
            longitude: 73.135,
          }}
          title={'Faisalabad'}
        />
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  mapStyles: {
    flex: 1,
  },
});

export default App;
